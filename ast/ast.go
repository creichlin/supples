package ast

import (
	"fmt"
)

type AST struct {
	Entities []*Entity
}

func (a *AST) FindEntity(name string) (*Entity, error) {
	for _, e := range a.Entities {
		if e.Name == name {
			return e, nil
		}
	}
	return nil, fmt.Errorf("entity %v not found", name)
}

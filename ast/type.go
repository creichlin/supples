package ast

type Type interface {
	IsAuto() bool
}

type Int struct {
}

func (i *Int) IsAuto() bool {
	return false
}

type Serial struct {
}

func (s *Serial) IsAuto() bool {
	return true
}

type Float struct {
}

func (f *Float) IsAuto() bool {
	return false
}

type String struct {
}

func (s *String) IsAuto() bool {
	return false
}

type Bool struct {
}

func (b *Bool) IsAuto() bool {
	return false
}

type Enum struct {
	Name   string
	Values []EnumValue
}

type EnumValue struct {
	ID    string
	Value string
}

func (b *Enum) IsAuto() bool {
	return false
}

type Time struct {
}

func (t *Time) IsAuto() bool {
	return false
}

type Reference struct {
	Name   string
	Entity *Entity
}

func (r *Reference) IsAuto() bool {
	return false
}

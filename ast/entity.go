package ast

import "strings"

type Entity struct {
	Name string

	// Attributes are all attributes defined on this entity including ID
	Attributes Attributes

	InverseAttributes Attributes
}

type Attribute struct {
	Name        string
	InverseName string
	Identity    bool
	Unique      bool
	Type        Type
}

func (a *Attribute) Resolve() []ResolvedAttribute {
	return Attributes{a}.Resolve()
}

type Attributes []*Attribute

func (a Attributes) NoIDs() Attributes {
	return a.filter(func(att *Attribute) bool { return !att.Identity })
}

func (a Attributes) OnlyIDs() Attributes {
	return a.filter(func(att *Attribute) bool { return att.Identity })
}

func (a Attributes) NoAuto() Attributes {
	return a.filter(func(att *Attribute) bool { return !att.Type.IsAuto() })
}

func (a Attributes) OnlyAuto() Attributes {
	return a.filter(func(att *Attribute) bool { return att.Type.IsAuto() })
}

func (a Attributes) OnlyUnique() Attributes {
	return a.filter(func(att *Attribute) bool { return att.Unique })
}

func (a Attributes) OnlyReferences() Attributes {
	return a.filter(func(att *Attribute) bool { _, is := att.Type.(*Reference); return is })
}

func (a Attributes) NoReferences() Attributes {
	return a.filter(func(att *Attribute) bool { _, is := att.Type.(*Reference); return !is })
}

func (a Attributes) filter(f func(a *Attribute) bool) Attributes {
	atts := Attributes{}
	for _, att := range a {
		if f(att) {
			atts = append(atts, att)
		}
	}
	return atts
}

func (atts Attributes) Resolve() []ResolvedAttribute {
	fats := []ResolvedAttribute{}
	for _, att := range atts {
		if ref, is := att.Type.(*Reference); is {
			for _, subFats := range ref.Entity.Attributes.OnlyIDs().Resolve() {
				afats := ResolvedAttribute{att}
				afats = append(afats, subFats...)
				fats = append(fats, afats)
			}
		} else {
			fats = append(fats, ResolvedAttribute{att})
		}
	}
	return fats
}

type ResolvedAttribute []*Attribute

func (ra ResolvedAttribute) Name() string {
	s := ""

	for _, a := range ra {
		s += strings.ToUpper(a.Name[:1]) + a.Name[1:]
	}
	return s
}

func (ra ResolvedAttribute) Type() Type {
	return ra[len(ra)-1].Type
}

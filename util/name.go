package util

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/jinzhu/inflection"
)

const (
	allowedChars      = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	allowedFirstChars = "abcdefghijklmnopqrstuvwxyz"
	upperChars        = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
)

type Name []string

func NewName(n string) (Name, error) {
	if n == "" {
		return nil, errors.New("names must not be the empty string")
	}

	if !strings.Contains(allowedFirstChars, string(n)[:1]) {
		return nil, fmt.Errorf("invalid name '%v' must start with one of '%v'", n, allowedFirstChars)
	}

	for _, c := range strings.Split(string(n[1:]), "") {
		if !strings.Contains(allowedChars, c) {
			return nil, fmt.Errorf("invalid name '%v' must only contain characters '%v'", n, allowedChars)
		}
	}

	p := []string{}

	from := 0
	for to := 2; to <= len(n); to++ {
		c := string(n[to-1 : to])
		if strings.Contains(upperChars, c) {
			p = append(p, strings.ToLower(string(n[from:to-1])))
			from = to - 1
		}
	}
	p = append(p, strings.ToLower(string(n[from:])))

	return Name(p), nil
}

func (n Name) Combine(others ...Name) Name {
	all := append([]string{}, n...)

	for _, p := range others {
		all = append(all, p...)
	}

	return Name(all)
}

func (n Name) Format(sep string, f func(int, string) string) string {
	npts := []string{}
	for i, v := range n {
		npts = append(npts, f(i, v))
	}
	return strings.Join(npts, sep)
}

func (n Name) Kebab() string {
	return n.Format("-", func(i int, s string) string {
		return s
	})
}

func (n Name) Str() string {
	return n.Format("", func(i int, s string) string {
		if i == 0 {
			return s
		}
		return strings.ToUpper(s[:1]) + s[1:]
	})
}

func (n Name) Camel() string {
	return n.Format("", func(i int, s string) string {
		return strings.ToUpper(s[:1]) + s[1:]
	})
}

func (n Name) Plural() Name {
	nstr := append([]string{}, n...)
	s := nstr[len(nstr)-1]
	nstr = nstr[:len(nstr)-1]

	p := inflection.Plural(s)
	if s == p {
		nstr = append(nstr, "multiple")
	}
	nstr = append(nstr, p)
	return Name(nstr)
}

func (n Name) Equal(o Name) bool {
	if n == nil && o == nil {
		return true
	}
	if n == nil || o == nil {
		return false
	}
	if len(n) != len(o) {
		return false
	}

	for i := range n {
		if n[i] != o[i] {
			return false
		}
	}
	return true
}

func (d *Name) UnmarshalJSON(data []byte) error {
	str := ""
	if err := json.Unmarshal(data, &str); err != nil {
		return err
	}
	nd, err := NewName(str)
	if err != nil {
		return err
	}
	*d = nd
	return nil
}

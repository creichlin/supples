package supples

import (
	"os"
	"path/filepath"

	"gitlab.com/creichlin/supples/ast"
	"gitlab.com/creichlin/supples/generate"
	"gitlab.com/creichlin/supples/parser"
)

type Generator struct {
	ast  *ast.AST
	ipkg string
}

func Generate(source []byte, path string) (*Generator, error) {
	ast, err := parser.Parse(string(source))
	if err != nil {
		return nil, err
	}

	err = os.MkdirAll(path, 0700)
	if err != nil {
		return nil, err
	}

	ipkg := filepath.Base(path)
	vars := map[string]interface{}{
		"ipkg": ipkg,
	}

	err = generate.Generate(ast, filepath.Join(path, "interface.gen.go"), "iface/interface.gg", vars)
	if err != nil {
		return nil, err
	}
	err = generate.Generate(ast, filepath.Join(path, "dao.gen.go"), "iface/dao.gg", vars)
	if err != nil {
		return nil, err
	}

	return &Generator{
		ast:  ast,
		ipkg: ipkg,
	}, nil
}

func (g *Generator) Test(path string) error {
	err := os.MkdirAll(path, 0700)
	if err != nil {
		return err
	}

	vars := map[string]interface{}{
		"ipkg": g.ipkg,
		"pkg":  filepath.Base(path),
	}
	err = generate.Generate(g.ast, filepath.Join(path, "store.gen.go"), "test/store.gg", vars)
	if err != nil {
		return err
	}

	return generate.Generate(g.ast, filepath.Join(path, "operation.gen.go"), "test/operation.gg", vars)
}

func (g *Generator) Postgres(path string) error {
	err := os.MkdirAll(path, 0700)
	if err != nil {
		return err
	}

	vars := map[string]interface{}{
		"ipkg": g.ipkg,
		"pkg":  filepath.Base(path),
	}
	err = generate.Generate(g.ast, filepath.Join(path, "postgres.gen.go"), "postgres/postgres.gg", vars, generate.PostgresNameFilter)
	if err != nil {
		return err
	}
	err = generate.Generate(g.ast, filepath.Join(path, "operation.gen.go"), "postgres/operation.gg", vars, generate.PostgresNameFilter)
	if err != nil {
		return err
	}
	err = generate.Generate(g.ast, filepath.Join(path, "query.gen.go"), "postgres/query.gg", vars, generate.PostgresNameFilter)
	if err != nil {
		return err
	}
	err = generate.Generate(g.ast, filepath.Join(path, "create.gen.sql"), "postgres/create.gg", vars, generate.PostgresNameFilter)
	if err != nil {
		return err
	}
	return generate.Generate(g.ast, filepath.Join(path, "drop.gen.sql"), "postgres/drop.gg", vars, generate.PostgresNameFilter)
}

func (g *Generator) MySQL(path string) error {
	err := os.MkdirAll(path, 0700)
	if err != nil {
		return err
	}

	vars := map[string]interface{}{
		"ipkg": g.ipkg,
		"pkg":  filepath.Base(path),
	}
	err = generate.Generate(g.ast, filepath.Join(path, "mysql.gen.go"), "mysql/mysql.gg", vars, generate.MySQLNameFilter)
	if err != nil {
		return err
	}
	err = generate.Generate(g.ast, filepath.Join(path, "operation.gen.go"), "mysql/operation.gg", vars, generate.MySQLNameFilter)
	if err != nil {
		return err
	}
	err = generate.Generate(g.ast, filepath.Join(path, "query.gen.go"), "mysql/query.gg", vars, generate.PostgresNameFilter)
	if err != nil {
		return err
	}
	err = generate.Generate(g.ast, filepath.Join(path, "create.gen.sql"), "mysql/create.gg", vars, generate.MySQLNameFilter)
	if err != nil {
		return err
	}
	return generate.Generate(g.ast, filepath.Join(path, "drop.gen.sql"), "mysql/drop.gg", vars, generate.MySQLNameFilter)
}

module gitlab.com/creichlin/supples

go 1.14

require (
	github.com/antlr/antlr4 v0.0.0-20200712162734-eb1adaa8a7a6
	github.com/ghodss/yaml v1.0.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/jinzhu/inflection v1.0.0
	github.com/lib/pq v1.7.0
	github.com/pkg/errors v0.9.1
	gitlab.com/creichlin/fmtid v0.0.0-20200423132658-0b9e6ed7c36f
	gitlab.com/creichlin/gogen v0.0.0-20201103160732-4f71b6cb77c7
	gitlab.com/creichlin/iotool v0.0.0-20201003173852-7df99b67c541
	gitlab.com/testle/expect v0.0.0-20200622171625-f00c39dcf2c4
	golang.org/x/tools v0.0.0-20200702044944-0cc1aa72b347
)

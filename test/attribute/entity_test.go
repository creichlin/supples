package main

import (
	"fmt"

	"gitlab.com/creichlin/supples"
	"gitlab.com/creichlin/supples/test/attribute/store"
	"gitlab.com/creichlin/supples/test/attribute/test"
	"gitlab.com/testle/expect"
)

func init() {
	run("select empty result must not return an error", func(ts *test.TestStore) {
		ts.MustDo("", func(o *test.TO) {
			o.SelectEntity().ExpectGet().ToBe([]*store.Entity{})
		})
	})

	run("get missing entity must return an error", func(ts *test.TestStore) {
		ts.MustDo("", func(o *test.TO) {
			o.ExpectGetEntityError(store.EntityID{ID: 4}).ToBe(&supples.NotFound{
				Entity:    "Entity",
				Condition: "id = {4}",
			})
		})
	})

	run("get missing entity must return an error message", func(ts *test.TestStore) {
		ts.MustDo("", func(o *test.TO) {
			o.ExpectGetEntityErrorMessage(store.EntityID{ID: 4}).ToBe("entity Entity not found by condition id = {4}")
		})
	})

	run("import entity", func(ts *test.TestStore) {
		ts.MustDo("import", func(o *test.TO) {
			o.MustImportEntity(foo2002)
			o.ExpectGetEntity(foo2002.ID).ToBe(&foo2002)
		})
	})

	run("import entityes", func(ts *test.TestStore) {
		ts.MustDo("import", func(o *test.TO) {
			o.MustImportEntities(foo2002, foo2003)
			o.ExpectGetEntity(foo2002.ID).ToBe(&foo2002)
			o.ExpectGetEntity(foo2003.ID).ToBe(&foo2003)
			o.SelectEntity().ExpectGet().ToCount(2)
		})
	})

	run("import sets auto id", func(ts *test.TestStore) {
		ts.MustDo("import", func(o *test.TO) {
			o.MustImportEntity(foo1)
			o.ExpectGetEntity(foo1.ID).ToBe(&foo1)
			o2 := o.MustCreateEntity(store.Entity{Name: "bar"})
			expect.Value(ts.T, "auto id", o2.ID).ToBe(foo1.ID.ID + 1)
		})
	})

	run("get by id", func(ts *test.TestStore) {
		ts.MustDo("get by id", func(o *test.TO) {
			o.MustImportEntity(foo1)
			o.ExpectGetEntity(foo1.ID).
				ToBe(&foo1)
		})
	})

	run("get by ids", func(ts *test.TestStore) {
		ts.MustDo("get by ids", func(o *test.TO) {
			o.MustImportEntity(foo1)
			o.MustImportEntity(foo2002)
			o.MustImportEntity(foo2003)

			o.SelectEntity().ByIDs(foo1.ID, foo2002.ID).ExpectGet().ToBe([]*store.Entity{&foo1, &foo2002})
			o.SelectEntity().ByIDs(foo1.ID, foo1.ID).ExpectGet().ToBe([]*store.Entity{&foo1})
			o.SelectEntity().ByIDs(foo2002.ID, foo2003.ID).ExpectGet().ToBe([]*store.Entity{&foo2002, &foo2003})
			o.SelectEntity().ByIDs().ExpectGet().ToBe([]*store.Entity{})
		})
	})

	run("delete entity by query", func(ts *test.TestStore) {
		ts.MustDo("", func(o *test.TO) {
			o.MustCreateEntity(store.Entity{Name: "foo"})
			n8 := o.MustCreateEntity(store.Entity{Name: "bar"})

			o.SelectEntity().NameIs("foo").MustDelete()
			o.SelectEntity().ExpectGetIDs().ToBe([]store.EntityID{n8})

			o.SelectEntity().MustDelete()
			o.SelectEntity().ExpectGet().ToCount(0)
		})
	})

	run("delete by id", func(ts *test.TestStore) {
		ts.MustDo("", func(o *test.TO) {
			n7 := o.MustCreateEntity(store.Entity{})
			n8 := o.MustCreateEntity(store.Entity{})

			o.MustDeleteEntity(n8)
			o.SelectEntity().ExpectGetIDs().ToBe([]store.EntityID{n7})
			o.MustDeleteEntity(n7)
			o.SelectEntity().ExpectGetIDs().ToBe([]store.EntityID{})
		})
	})

	run("count by select", func(ts *test.TestStore) {
		ts.MustDo("", func(o *test.TO) {
			o.MustCreateEntity(mkEntity(1))
			o.MustCreateEntity(mkEntity(2))
			o.MustCreateEntity(mkEntity(3))
			o.MustCreateEntity(mkEntity(4))
			o.MustCreateEntity(mkEntity(5))
			o.MustCreateEntity(mkEntity(6))
			o.MustCreateEntity(mkEntity(6))

			o.SelectEntity().ExpectCount().ToBe(7)
			o.SelectEntity().NameIs("foo6").ExpectCount().ToBe(2)
		})
	})

	run("limit", func(ts *test.TestStore) {
		ts.MustDo("", func(o *test.TO) {
			e1 := o.MustCreateEntity(mkEntity(1))
			e2 := o.MustCreateEntity(mkEntity(2))
			e3 := o.MustCreateEntity(mkEntity(3))
			e4 := o.MustCreateEntity(mkEntity(4))
			e5 := o.MustCreateEntity(mkEntity(5))
			e6 := o.MustCreateEntity(mkEntity(6))

			o.SelectEntity().ExpectCount().ToBe(6)
			o.SelectEntity().Limit(0, 1).ExpectGetIDs().ToBe([]store.EntityID{e1})
			o.SelectEntity().Limit(1, 5).ExpectGetIDs().ToBe([]store.EntityID{e2, e3, e4, e5})
			o.SelectEntity().Limit(4, 5).ExpectGetIDs().ToBe([]store.EntityID{e5})
			o.SelectEntity().Limit(5, 100).ExpectGetIDs().ToBe([]store.EntityID{e6})
		})
	})

	run("limit param errors", func(ts *test.TestStore) {
		ts.MustDo("", func(o *test.TO) {
			o.SelectEntity().Limit(-1, 10).ExpectGetErrorMessage().ToBe("from value must be >= 0 but is -1")
			o.SelectEntity().Limit(20, 15).ExpectGetErrorMessage().ToBe("to value must be > from but it is 15 which is less than 20")
			o.SelectEntity().Limit(20, 20).ExpectGetErrorMessage().ToBe("to value must be > from but it is 20 which is the same as from")
		})
	})
}

func mkEntity(id int) store.Entity {
	return store.Entity{
		ID:   store.EntityID{ID: id},
		Name: fmt.Sprintf("foo%v", id),
	}
}

var foo2002 = store.Entity{
	ID:   store.EntityID{ID: 2002},
	Name: "foo",
}
var foo2003 = store.Entity{
	ID:   store.EntityID{ID: 2003},
	Name: "foo2003",
}
var foo1 = store.Entity{
	ID:   store.EntityID{ID: 12},
	Name: "foo",
}

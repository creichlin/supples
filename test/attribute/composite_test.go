package main

import (
	"gitlab.com/creichlin/supples/test/attribute/store"
	"gitlab.com/creichlin/supples/test/attribute/test"
)

func init() {
	run("order by reference", func(ts *test.TestStore) {
		ts.MustDo("", func(o *test.TO) {
			id1 := o.MustCreateComposite(store.Composite{ID: store.CompositeID{Name: "baz", Year: 1}})
			id2 := o.MustCreateComposite(store.Composite{ID: store.CompositeID{Name: "foo", Year: 1}})
			id3 := o.MustCreateComposite(store.Composite{ID: store.CompositeID{Name: "bar", Year: 2}})

			t1 := o.MustCreateCompositeTag(store.CompositeTag{ID: store.CompositeTagID{Composite: id1}})
			t2 := o.MustCreateCompositeTag(store.CompositeTag{ID: store.CompositeTagID{Composite: id2}})
			t3 := o.MustCreateCompositeTag(store.CompositeTag{ID: store.CompositeTagID{Composite: id3}})

			o.SelectCompositeTag().OrderCompositeNameDesc().ExpectGetIDs().ToBe([]store.CompositeTagID{t2, t3, t1})
		})
	})
}

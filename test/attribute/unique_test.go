package main

import (
	"time"

	"gitlab.com/creichlin/supples/test/attribute/store"
	"gitlab.com/creichlin/supples/test/attribute/test"
)

func init() {
	run("unique select", func(ts *test.TestStore) {
		un := store.Unique{
			Float:  17.04,
			Int:    128,
			String: "foo",
			Time:   time.Unix(123456789, 345678000).UTC(),
		}

		ts.MustDo("unique select", func(o *test.TO) {
			id := o.MustCreateUnique(un)
			un.ID = id

			o.MustCreateUnique(store.Unique{
				Float:  17.0467,
				Int:    129,
				String: "bar",
				Time:   time.Unix(123456789, 345678002).UTC(),
			})

			o.ExpectGetUniqueByFloat(17.04).ToBe(&un)
			o.ExpectGetUniqueByString("foo").ToBe(&un)
			o.ExpectGetUniqueByInt(128).ToBe(&un)
			o.ExpectGetUniqueByTime(time.Unix(123456789, 345678000).UTC()).ToBe(&un)
		})
	})
}

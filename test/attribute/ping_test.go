package main

import (
	"gitlab.com/creichlin/supples/test/attribute/test"
	"gitlab.com/testle/expect"
)

func init() {
	run("ping test", func(ts *test.TestStore) {
		expect.Value(ts.T, "ping response", ts.Store.Ping()).ToBe(nil)
	})
}

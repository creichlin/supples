package main

import (
	"testing"

	"gitlab.com/creichlin/supples/test/attribute/mysql"
	"gitlab.com/creichlin/supples/test/attribute/postgres"
	"gitlab.com/creichlin/supples/test/attribute/test"
)

type tc struct {
	name string
	test func(ts *test.TestStore)
}

var tests = []tc{}

func run(name string, t func(t *test.TestStore)) {
	tests = append(tests, tc{name, t})
}

func TestAll(t *testing.T) {

	for _, tc := range tests {
		t.Run("postgres "+tc.name, func(t *testing.T) {
			pdb, err := postgres.New("host=localhost port=5432 user=postgres password=postgres dbname=postgres sslmode=disable")
			if err != nil {
				t.Fatal(err)
			}
			ts := test.New(t, pdb)
			tc.test(ts)
		})
		t.Run("mysql "+tc.name, func(t *testing.T) {
			mdb, err := mysql.New("mysql:mysql@tcp(127.0.0.1)/mysql")
			if err != nil {
				t.Fatal(err)
			}
			ts := test.New(t, mdb)
			tc.test(ts)
		})
	}
}

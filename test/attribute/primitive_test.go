package main

import (
	"time"

	"gitlab.com/creichlin/supples/test/attribute/store"
	"gitlab.com/creichlin/supples/test/attribute/test"
)

func init() {
	run("create and get primitives", func(ts *test.TestStore) {
		ts.MustDo("primitives", func(o *test.TO) {
			id := o.MustCreatePrimitive(store.Primitive{
				ID:     store.PrimitiveID{},
				Enum:   store.PrimitiveEnumDefault,
				Float:  17.0455665,
				Int:    -124,
				Bool:   true,
				String: "fooo-bar\nbaaar",
				Time:   time.Unix(123456789, 345678000),
			})

			o.ExpectGetPrimitive(id).ToBe(&store.Primitive{
				ID:     id,
				Enum:   store.PrimitiveEnumDefault,
				Float:  17.0455665,
				Int:    -124,
				Bool:   true,
				String: "fooo-bar\nbaaar",
				Time:   time.Unix(123456789, 345678000).UTC(),
			})
		})
	})

	run("update primitives", func(ts *test.TestStore) {
		prim := store.Primitive{
			Enum:   store.PrimitiveEnumDefault,
			Float:  17.0455665,
			Int:    -124,
			Bool:   true,
			String: "fooo-bar\nbaaar",
			Time:   time.Unix(123456789, 345678000).UTC(),
		}

		ts.MustDo("primitives", func(o *test.TO) {
			prim.ID = o.MustCreatePrimitive(store.Primitive{Enum: store.PrimitiveEnumDefault})

			o.MustUpdatePrimitive(prim)

			o.ExpectGetPrimitive(prim.ID).ToBe(&prim)
		})
	})

	run("query by isAttribute", func(ts *test.TestStore) {
		ts.MustDo("primitives", func(o *test.TO) {
			id := o.MustCreatePrimitive(store.Primitive{
				Enum:   store.PrimitiveEnumFoo,
				Float:  17.0455665,
				Int:    -124,
				Bool:   true,
				String: "FOO",
				Time:   time.Unix(123456789, 345678000),
			})
			o.MustCreatePrimitive(store.Primitive{
				Enum: store.PrimitiveEnumDefault,
			})

			o.SelectPrimitive().IntIs(-124).ExpectGetIDs().ToBe([]store.PrimitiveID{id})
			o.SelectPrimitive().BoolIs(true).ExpectGetIDs().ToBe([]store.PrimitiveID{id})
			o.SelectPrimitive().StringIs("FOO").ExpectGetIDs().ToBe([]store.PrimitiveID{id})
			o.SelectPrimitive().EnumIs(store.PrimitiveEnumFoo).ExpectGetIDs().ToBe([]store.PrimitiveID{id})
			o.SelectPrimitive().FloatIs(17.0455665).ExpectGetIDs().ToBe([]store.PrimitiveID{id})
			o.SelectPrimitive().TimeIs(time.Unix(123456789, 345678000)).ExpectGetIDs().ToBe([]store.PrimitiveID{id})
		})
	})

	run("query by like string", func(ts *test.TestStore) {
		ts.MustDo("primitives", func(o *test.TO) {
			f := o.MustCreatePrimitive(store.Primitive{String: "FOO"})
			b := o.MustCreatePrimitive(store.Primitive{String: "BAR"})
			z := o.MustCreatePrimitive(store.Primitive{String: "BAZ"})
			fbz := o.MustCreatePrimitive(store.Primitive{String: "FOOBARBAZ"})

			o.SelectPrimitive().StringLike("FOO%").ExpectGetIDs().ToBe([]store.PrimitiveID{f, fbz})
			o.SelectPrimitive().StringLike("BA_").ExpectGetIDs().ToBe([]store.PrimitiveID{b, z})
			o.SelectPrimitive().StringLike("%BAR%").ExpectGetIDs().ToBe([]store.PrimitiveID{b, fbz})
		})
	})

	run("query by date comparison", func(ts *test.TestStore) {
		ts.MustDo("query by date comparison", func(o *test.TO) {
			t := time.Unix(123456789, 345678000)
			after := t.Add(time.Second)
			before := t.Add(-time.Second)
			id := o.MustCreatePrimitive(store.Primitive{
				Time: time.Unix(123456789, 345678000),
			})

			o.SelectPrimitive().TimeIsGreaterEqual(t).ExpectGetIDs().ToBe([]store.PrimitiveID{id})
			o.SelectPrimitive().TimeIsGreaterEqual(after).ExpectGetIDs().ToBe([]store.PrimitiveID{})

			o.SelectPrimitive().TimeIsSmallerEqual(t).ExpectGetIDs().ToBe([]store.PrimitiveID{id})
			o.SelectPrimitive().TimeIsSmallerEqual(before).ExpectGetIDs().ToBe([]store.PrimitiveID{})

			o.SelectPrimitive().TimeIsGreater(t).ExpectGetIDs().ToBe([]store.PrimitiveID{})
			o.SelectPrimitive().TimeIsGreater(before).ExpectGetIDs().ToBe([]store.PrimitiveID{id})

			o.SelectPrimitive().TimeIsSmaller(t).ExpectGetIDs().ToBe([]store.PrimitiveID{})
			o.SelectPrimitive().TimeIsSmaller(after).ExpectGetIDs().ToBe([]store.PrimitiveID{id})
		})
	})

	run("sorted by date", func(ts *test.TestStore) {
		ts.MustDo("", func(o *test.TO) {
			t1 := time.Unix(123456789, 345678000)
			t2 := t1.Add(time.Second)
			t3 := t1.Add(time.Second * 1000)
			id2 := o.MustCreatePrimitive(store.Primitive{Time: t2})
			id1 := o.MustCreatePrimitive(store.Primitive{Time: t1})
			id3 := o.MustCreatePrimitive(store.Primitive{Time: t3})

			o.SelectPrimitive().OrderTimeAsc().ExpectGetIDs().ToBe([]store.PrimitiveID{id1, id2, id3})
			o.SelectPrimitive().OrderTimeDesc().ExpectGetIDs().ToBe([]store.PrimitiveID{id3, id2, id1})
		})
	})
}

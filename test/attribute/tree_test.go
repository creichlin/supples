package main

import (
	"fmt"

	"gitlab.com/creichlin/supples/test/attribute/store"
	"gitlab.com/creichlin/supples/test/attribute/test"
)

func init() {
	run("find leafs by trunk", func(ts *test.TestStore) {
		ts.MustDo("find leafs by trunk", func(op *test.TO) {
			t1 := createTreeStruct(op, "foo", 3)
			t2 := createTreeStruct(op, "bar", 2)
			op.SelectLeaf().JoinBranch(op.SelectBranch().WithStem(t1.ID)).ExpectGet().ToCount(9)
			op.SelectLeaf().JoinBranch(op.SelectBranch().WithStem(t2.ID)).ExpectGet().ToCount(4)
		})
	})

	run("find trunk by leaf", func(ts *test.TestStore) {
		ts.MustDo("find trunk by leaf", func(op *test.TO) {
			t1 := createTreeStruct(op, "foo", 3)
			t2 := createTreeStruct(op, "bar", 2)

			op.SelectTrunk().JoinBranch(op.SelectBranch().JoinLeaf(op.SelectLeaf().NameIs("foo_1_1"))).
				ExpectGet().ToBe([]*store.Trunk{&t1})
			op.SelectTrunk().JoinBranch(op.SelectBranch().JoinLeaf(op.SelectLeaf().NameIs("bar_0_1"))).
				ExpectGet().ToBe([]*store.Trunk{&t2})
		})
	})

	run("find leafs by trunks", func(ts *test.TestStore) {
		ts.MustDo("find trunk by leaf", func(op *test.TO) {
			t1 := createTreeStruct(op, "foo", 3)
			t2 := createTreeStruct(op, "bar", 2)
			t3 := createTreeStruct(op, "baz", 1)

			op.SelectLeaf().JoinBranch(op.SelectBranch().WithStems(t1.ID, t2.ID)).ExpectGet().ToCount(13)
			op.SelectLeaf().JoinBranch(op.SelectBranch().WithStems(t2.ID, t3.ID)).ExpectGet().ToCount(5)
			op.SelectLeaf().JoinBranch(op.SelectBranch().WithStems(t2.ID, t3.ID, t1.ID)).ExpectGet().ToCount(14)
		})
	})

}

func createTreeStruct(op *test.TO, name string, n int) store.Trunk {
	t := store.Trunk{Name: name}
	t.ID = op.MustCreateTrunk(t)
	for i := 0; i < n; i++ {
		b := store.Branch{
			Stem: t.ID,
			Name: fmt.Sprintf("%v_%v", name, i),
		}
		b.ID = op.MustCreateBranch(b)
		for j := 0; j < n; j++ {
			l := store.Leaf{
				Branch: b.ID,
				Name:   fmt.Sprintf("%v_%v_%v", name, i, j),
			}
			op.MustCreateLeaf(l)
		}
	}
	return t
}

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"time"

	"gitlab.com/creichlin/supples"
)

func main() {
	start := time.Now()
	_, filename, _, _ := runtime.Caller(0)
	root := path.Dir(path.Dir(path.Dir(filename)))

	files, err := ioutil.ReadDir(root)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		if f.IsDir() {
			data, err := ioutil.ReadFile(filepath.Join(root, f.Name(), f.Name()+".pg"))
			if os.IsNotExist(err) {
				continue
			}
			if err != nil {
				log.Fatal(err)
			}

			gen, err := supples.Generate(data, filepath.Join(root, f.Name(), "store"))
			if err != nil {
				log.Fatal(err)
			}

			err = gen.Postgres(filepath.Join(root, f.Name(), "postgres"))
			if err != nil {
				log.Fatal(err)
			}

			err = gen.MySQL(filepath.Join(root, f.Name(), "mysql"))
			if err != nil {
				log.Fatal(err)
			}

			err = gen.Test(filepath.Join(root, f.Name(), "test"))
			if err != nil {
				log.Fatal(err)
			}
		}
	}

	fmt.Println(time.Since(start))
}

package main

import (
	"gitlab.com/creichlin/supples/test/join/store"
	"gitlab.com/creichlin/supples/test/join/test"
)

func init() {
	// run("test simple join query", func(ts *test.TestStore) {
	// 	ts.MustDo("test select query", func(o *test.TO) {
	// 		o.SelectComment().JoinDocument(
	// 			o.SelectDocument().WithAuthor(store.UserID{ID: 7}),
	// 		).ExpectQuery().
	// 			ToBe("SELECT comment__1.document_id, comment__1.id, comment__1.author_id FROM comment AS comment__1, document AS document__2 " +
	// 				"WHERE (comment__1.document_id = document__2.id) AND (document__2.author_id = $1)")
	// 	})
	// })

	run("test simple join", func(ts *test.TestStore) {
		ts.MustDo("test join", func(o *test.TO) {
			userFoo := o.MustCreateUser(store.User{Login: "foo"})
			userBar := o.MustCreateUser(store.User{Login: "bar"})

			doc1 := o.MustCreateDocument(store.Document{Author: userFoo})
			doc2 := o.MustCreateDocument(store.Document{Author: userBar})

			comment1 := o.MustCreateComment(store.Comment{Author: userBar, ID: store.CommentID{Document: doc1}})
			o.MustCreateComment(store.Comment{Author: userBar, ID: store.CommentID{Document: doc2}})

			o.SelectComment().JoinDocument(
				o.SelectDocument().WithAuthor(userFoo),
			).ExpectGet().ToBe([]*store.Comment{{ID: comment1, Author: userBar}})
		})
	})

	run("test n2n join", func(ts *test.TestStore) {
		ts.MustDo("test n to n join", func(o *test.TO) {
			userFoo := o.MustCreateUser(store.User{Login: "foo"})
			userBar := o.MustCreateUser(store.User{Login: "bar"})

			doc1 := o.MustCreateDocument(store.Document{Author: userFoo})
			doc2 := o.MustCreateDocument(store.Document{Author: userBar})

			o.MustCreateRight(store.Right{
				ID:       store.RightID{User: userFoo, Document: doc1},
				Function: store.FunctionEditor,
			})
			o.MustCreateRight(store.Right{
				ID:       store.RightID{User: userFoo, Document: doc2},
				Function: store.FunctionEditor,
			})

			o.MustCreateRight(store.Right{
				ID:       store.RightID{User: userBar, Document: doc1},
				Function: store.FunctionEditor,
			})

			o.SelectUser().JoinRight(
				o.SelectRight().FunctionIs(store.FunctionEditor).WithDocument(doc1),
			).ExpectGetIDs().ToBe([]store.UserID{userFoo, userBar})

			o.SelectUser().JoinRight(
				o.SelectRight().FunctionIs(store.FunctionEditor).WithDocument(doc2),
			).ExpectGetIDs().ToBe([]store.UserID{userFoo})
		})
	})
}

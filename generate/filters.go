package generate

import (
	"gitlab.com/creichlin/fmtid"
	"gitlab.com/creichlin/gogen"
	"gitlab.com/creichlin/supples/ast"
)

var Filters = []gogen.Option{}

func init() {
	for k, fn := range fmtid.Formats {
		Filters = append(Filters, gogen.FilterOption(k, fn))
	}
}

func filter(atts []*ast.Attribute, f func(a *ast.Attribute) bool) []*ast.Attribute {
	as := []*ast.Attribute{}
	for _, a := range atts {
		if f(a) {
			as = append(as, a)
		}
	}
	return as
}

var PostgresNameFilter = gogen.FilterOption("sql", sqlPostgres)
var MySQLNameFilter = gogen.FilterOption("sql", sqlMySQL)

func sqlPostgres(i string) (string, error) {
	l, err := fmtid.Snake(i)
	if err != nil {
		return "", err
	}
	if _, has := reservedPostgres[l]; has {
		return "db_" + l, nil
	}
	return l, nil
}

func sqlMySQL(i string) (string, error) {
	l, err := fmtid.Snake(i)
	if err != nil {
		return "", err
	}
	if _, has := reservedMySQL[l]; has {
		return "db_" + l, nil
	}
	return l, nil
}

var reservedPostgres = func() map[string]bool {
	// keyword list generated by:
	//  select word from pg_get_keywords() WHERE catcode in ('R', 'T');
	list := []string{
		"all",
		"analyse",
		"analyze",
		"and",
		"any",
		"array",
		"as",
		"asc",
		"asymmetric",
		"authorization",
		"binary",
		"both",
		"case",
		"cast",
		"check",
		"collate",
		"collation",
		"column",
		"concurrently",
		"constraint",
		"create",
		"cross",
		"current_catalog",
		"current_date",
		"current_role",
		"current_schema",
		"current_time",
		"current_timestamp",
		"current_user",
		"default",
		"deferrable",
		"desc",
		"distinct",
		"do",
		"else",
		"end",
		"except",
		"false",
		"fetch",
		"for",
		"foreign",
		"freeze",
		"from",
		"full",
		"grant",
		"group",
		"having",
		"ilike",
		"in",
		"initially",
		"inner",
		"intersect",
		"into",
		"is",
		"isnull",
		"join",
		"lateral",
		"leading",
		"left",
		"like",
		"limit",
		"localtime",
		"localtimestamp",
		"natural",
		"not",
		"notnull",
		"null",
		"offset",
		"on",
		"only",
		"or",
		"order",
		"outer",
		"overlaps",
		"placing",
		"primary",
		"references",
		"returning",
		"right",
		"select",
		"session_user",
		"similar",
		"some",
		"symmetric",
		"table",
		"tablesample",
		"then",
		"to",
		"trailing",
		"true",
		"union",
		"unique",
		"user",
		"using",
		"variadic",
		"verbose",
		"when",
		"where",
		"window",
		"with",
	}

	m := map[string]bool{}
	for _, k := range list {
		m[k] = true
	}
	return m
}()

var reservedMySQL = func() map[string]bool {
	// keyword list generated by:
	//  select word from pg_get_keywords() WHERE catcode in ('R', 'T');
	list := []string{
		"int",
		"float",
		"function",
		// from postgres
		"all",
		"analyse",
		"analyze",
		"and",
		"any",
		"array",
		"as",
		"asc",
		"asymmetric",
		"authorization",
		"binary",
		"both",
		"case",
		"cast",
		"check",
		"collate",
		"collation",
		"column",
		"concurrently",
		"constraint",
		"create",
		"cross",
		"current_catalog",
		"current_date",
		"current_role",
		"current_schema",
		"current_time",
		"current_timestamp",
		"current_user",
		"default",
		"deferrable",
		"desc",
		"distinct",
		"do",
		"else",
		"end",
		"except",
		"false",
		"fetch",
		"for",
		"foreign",
		"freeze",
		"from",
		"full",
		"grant",
		"group",
		"having",
		"ilike",
		"in",
		"initially",
		"inner",
		"intersect",
		"into",
		"is",
		"isnull",
		"join",
		"lateral",
		"leading",
		"left",
		"like",
		"limit",
		"localtime",
		"localtimestamp",
		"natural",
		"not",
		"notnull",
		"null",
		"offset",
		"on",
		"only",
		"or",
		"order",
		"outer",
		"overlaps",
		"placing",
		"primary",
		"references",
		"returning",
		"right",
		"select",
		"session_user",
		"similar",
		"some",
		"symmetric",
		"table",
		"tablesample",
		"then",
		"to",
		"trailing",
		"true",
		"union",
		"unique",
		"user",
		"using",
		"variadic",
		"verbose",
		"when",
		"where",
		"window",
		"with",
	}

	m := map[string]bool{}
	for _, k := range list {
		m[k] = true
	}
	return m
}()

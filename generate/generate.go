package generate

import (
	"io/ioutil"
	"path/filepath"
	"runtime"
	"strings"
	"sync"

	"gitlab.com/creichlin/gogen"
	"gitlab.com/creichlin/gogen/parser"
	"gitlab.com/creichlin/iotool"
	"gitlab.com/creichlin/supples/ast"
)

var tplCache = map[string]*gogen.AST{}
var lock sync.Mutex

func getAST(dir, name string) (*gogen.AST, error) {
	path := filepath.Join(dir, name)

	lock.Lock()
	if gg, has := tplCache[path]; has {
		lock.Unlock()
		return gg, nil
	}
	lock.Unlock()

	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	sourceFolder := filepath.Dir(path)

	loadLib := func(name string) (string, string, error) {
		fp := filepath.Join(dir, "common", name+".gg")
		d, err := ioutil.ReadFile(fp)
		if err != nil {
			fp = filepath.Join(sourceFolder, name+".gg")
			d, err = ioutil.ReadFile(fp)
			if err != nil {
				return "", "", err
			}
		}
		return string(d), fp, nil
	}

	gg, err := parser.Parse(string(data), name, loadLib)
	if err != nil {
		return nil, err
	}
	lock.Lock()
	tplCache[path] = gg
	lock.Unlock()
	return gg, err
}

func Generate(ast *ast.AST, destFile, template string, vars map[string]interface{}, filters ...gogen.Option) error {
	_, filename, _, _ := runtime.Caller(0)
	dir := filepath.Dir(filename)

	gg, err := getAST(dir, template)
	if err != nil {
		return err
	}

	// d, _ := yaml.Marshal(gg)
	// fmt.Println(string(d))

	opts := append([]gogen.Option{}, Filters...)
	opts = append(opts, filters...)
	for n, v := range vars {
		opts = append(opts, gogen.WithVar(n, v))
	}
	result, err := gogen.Execute(gg, ast, opts...)
	if err != nil {
		panic(err)
	}

	if strings.HasSuffix(destFile, ".go") {
		result, err = goImports(destFile, result)
		if err != nil {
			return err
		}
	}
	return iotool.WriteChangedFile(destFile, []byte(result), 0600)
}

package supples

import "fmt"

type NotFound struct {
	Entity    string
	Condition string
}

func (n *NotFound) Error() string {
	return fmt.Sprintf("entity %v not found by condition %v", n.Entity, n.Condition)
}

package parser

import (
	"fmt"

	"gitlab.com/creichlin/supples/ast"
)

type ASTVisitor struct {
	*BaseSupplesVisitor
	error func(err error)
}

func (v *ASTVisitor) VisitRoot(ctx *RootContext) interface{} {
	a := &ast.AST{}

	for _, entityDec := range ctx.AllEntity() {
		e := entityDec.Accept(v).(*ast.Entity)
		a.Entities = append(a.Entities, e)
	}

	return a
}

func (v *ASTVisitor) VisitIdentifiers(ctx *IdentifiersContext) interface{} {
	ids := []string{}
	for _, id := range ctx.AllIdentifier() {
		ids = append(ids, id.GetText())
	}
	return ids
}

func (v *ASTVisitor) VisitEntity(ctx *EntityContext) interface{} {
	e := &ast.Entity{
		Name: ctx.Identifier().GetText(),
	}

	// find out identity attributes
	ids := []string{}
	if ctx.Identifiers() != nil {
		ids = ctx.Identifiers().Accept(v).([]string)
	}

	idm := map[string]bool{}
	for _, id := range ids {
		idm[id] = true
	}

	// if there are no identity attributes, add one called id
	if len(ids) == 0 {
		ids = []string{"id"}
		e.Attributes = append(e.Attributes, &ast.Attribute{
			Name:     "id",
			Type:     &ast.Serial{},
			Identity: true,
		})
	}

	for _, a := range ctx.AllAttribute() {
		at := a.Accept(v).(*ast.Attribute)
		if idm[at.Name] {
			at.Identity = true
			delete(idm, at.Name)
		}
		e.Attributes = append(e.Attributes, at)
	}

	for k := range idm { // check if we found all identity attributes
		v.error(fmt.Errorf("identity attribute %v not defined for entity %v", k, e.Name))
	}

	return e
}

func (v *ASTVisitor) VisitAttribute(ctx *AttributeContext) interface{} {

	att := &ast.Attribute{
		Name: ctx.Identifier().GetText(),
	}

	if ctx.TypeDef() != nil {
		att.Type = ctx.TypeDef().Accept(v).(ast.Type)
	}
	if ctx.UniqueTypeDef() != nil {
		att.Type = ctx.UniqueTypeDef().Accept(v).(ast.Type)
		att.Unique = ctx.UNIQUE() != nil
	}

	return att
}

func (v *ASTVisitor) VisitUniqueTypeDef(ctx *UniqueTypeDefContext) interface{} {
	if ctx.IntType() != nil {
		return ctx.IntType().Accept(v)
	}
	if ctx.FloatType() != nil {
		return ctx.FloatType().Accept(v)
	}
	if ctx.StringType() != nil {
		return ctx.StringType().Accept(v)
	}
	if ctx.TimeType() != nil {
		return ctx.TimeType().Accept(v)
	}

	panic("no type def")
}

func (v *ASTVisitor) VisitTypeDef(ctx *TypeDefContext) interface{} {
	if ctx.SerialType() != nil {
		return ctx.SerialType().Accept(v)
	}
	if ctx.BoolType() != nil {
		return ctx.BoolType().Accept(v)
	}
	if ctx.ReferenceType() != nil {
		return ctx.ReferenceType().Accept(v)
	}
	if ctx.EnumType() != nil {
		return ctx.EnumType().Accept(v)
	}

	panic("no type def")
}

func (v *ASTVisitor) VisitIntType(ctx *IntTypeContext) interface{} {
	return &ast.Int{}
}

func (v *ASTVisitor) VisitSerialType(ctx *SerialTypeContext) interface{} {
	return &ast.Serial{}
}

func (v *ASTVisitor) VisitStringType(ctx *StringTypeContext) interface{} {
	return &ast.String{}
}

func (v *ASTVisitor) VisitFloatType(ctx *FloatTypeContext) interface{} {
	return &ast.Float{}
}

func (v *ASTVisitor) VisitBoolType(ctx *BoolTypeContext) interface{} {
	return &ast.Bool{}
}

func (v *ASTVisitor) VisitEnumType(ctx *EnumTypeContext) interface{} {
	en := &ast.Enum{}
	for _, val := range ctx.AllEnumValue() {
		ev := val.Accept(v).(ast.EnumValue)
		en.Values = append(en.Values, ev)
	}
	return en
}

func (v *ASTVisitor) VisitEnumValue(ctx *EnumValueContext) interface{} {
	id := ctx.Identifier().GetText()
	value := id
	if ctx.STRING() != nil {
		value = ctx.STRING().GetText()
		value = value[1 : len(value)-1]
	}
	return ast.EnumValue{
		ID:    id,
		Value: value,
	}
}

func (v *ASTVisitor) VisitTimeType(ctx *TimeTypeContext) interface{} {
	return &ast.Time{}
}

func (v *ASTVisitor) VisitReferenceType(ctx *ReferenceTypeContext) interface{} {
	return &ast.Reference{
		Name: ctx.Identifier().GetText(),
	}
}

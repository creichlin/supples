// Code generated from parser/Supples.g4 by ANTLR 4.8. DO NOT EDIT.

package parser // Supples

import "github.com/antlr/antlr4/runtime/Go/antlr"

type BaseSupplesVisitor struct {
	*antlr.BaseParseTreeVisitor
}

func (v *BaseSupplesVisitor) VisitIdentifier(ctx *IdentifierContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseSupplesVisitor) VisitIdentifiers(ctx *IdentifiersContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseSupplesVisitor) VisitPkg(ctx *PkgContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseSupplesVisitor) VisitRoot(ctx *RootContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseSupplesVisitor) VisitEntity(ctx *EntityContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseSupplesVisitor) VisitAttribute(ctx *AttributeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseSupplesVisitor) VisitUniqueTypeDef(ctx *UniqueTypeDefContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseSupplesVisitor) VisitTypeDef(ctx *TypeDefContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseSupplesVisitor) VisitIntType(ctx *IntTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseSupplesVisitor) VisitSerialType(ctx *SerialTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseSupplesVisitor) VisitFloatType(ctx *FloatTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseSupplesVisitor) VisitStringType(ctx *StringTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseSupplesVisitor) VisitBoolType(ctx *BoolTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseSupplesVisitor) VisitEnumType(ctx *EnumTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseSupplesVisitor) VisitEnumValue(ctx *EnumValueContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseSupplesVisitor) VisitTimeType(ctx *TimeTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseSupplesVisitor) VisitReferenceType(ctx *ReferenceTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

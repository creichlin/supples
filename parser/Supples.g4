grammar Supples;

COL: ':';

UNIQUE: 'unique';

IDENTIFIER: [a-z][a-zA-Z0-9]*;

STRING: '"' .*? '"';

WHITESPACE: [ \r\n\t]+ -> skip;

identifier:
	IDENTIFIER
	| 'int'
	| 'float'
	| 'string'
	| 'enum'
	| 'bool'
	| 'time'
	// keywords
	| 'entity'
	| UNIQUE
	| 'identified'
	| 'by'
	| 'name'
	| 'interfacePackage'
	| 'postgresPackage';

identifiers: identifier (',' identifier)*;

pkg: identifier (('.' | '/') identifier)*;

root: (entity)* EOF;

entity:
	'entity' identifier ('identified' 'by' identifiers)? '{' attribute* '}';

attribute: identifier (UNIQUE? uniqueTypeDef | typeDef);

uniqueTypeDef: intType | floatType | stringType | timeType;

typeDef: serialType | boolType | enumType | referenceType;

intType: 'int';

serialType: 'serial';

floatType: 'float';

stringType: 'string';

boolType: 'bool';

enumType: 'enum' '(' enumValue (',' enumValue)+ ')';

enumValue: identifier ('=' STRING)?;

timeType: 'time';

referenceType: '->' identifier;
package parser

import (
	"fmt"
	"strings"

	"github.com/antlr/antlr4/runtime/Go/antlr"
	"gitlab.com/creichlin/supples/ast"
)

func Parse(i string) (*ast.AST, error) {
	input := antlr.NewInputStream(i)
	lexer := NewSupplesLexer(input)
	stream := antlr.NewCommonTokenStream(lexer, 0)

	p := NewSupplesParser(stream)
	p.RemoveErrorListeners()
	antlrErrors := NewErrorListener()
	p.AddErrorListener(antlrErrors)
	p.BuildParseTrees = true
	root := p.Root()

	visi := &ASTVisitor{
		BaseSupplesVisitor: &BaseSupplesVisitor{},
		error: func(e error) {
			antlrErrors.errors = append(antlrErrors.errors, e)
		},
	}

	result := root.Accept(visi).(*ast.AST)

	err := normalize(result)
	if err != nil {
		return nil, err
	}

	// d, _ := yaml.Marshal(result)
	// fmt.Println(string(d))

	return result, antlrErrors.Get()
}

func normalize(r *ast.AST) error {
	enumNames := map[string]int{}

	for _, e := range r.Entities {
		for _, a := range e.Attributes {
			if ref, is := a.Type.(*ast.Reference); is {
				refEntity, err := r.FindEntity(ref.Name)
				if err != nil {
					return fmt.Errorf("invalid reference %v.%v, %v", e.Name, a.Name, err)
				}
				ref.Entity = refEntity

				ref.Entity.InverseAttributes = append(ref.Entity.InverseAttributes, &ast.Attribute{
					Name: e.Name,
					Type: &ast.Reference{
						Entity: e,
						Name:   e.Name,
					},
					InverseName: a.Name,
				})
			}

			if _, is := a.Type.(*ast.Enum); is {
				enumNames[a.Name] = enumNames[a.Name] + 1
			}
		}
	}

	setEnumName := func(e *ast.Entity, a *ast.Attribute, t ast.Type) {
		if enum, is := t.(*ast.Enum); is {
			if enumNames[a.Name] > 1 {
				enum.Name = e.Name + strings.ToUpper(a.Name[0:1]) + a.Name[1:]
			} else {
				enum.Name = a.Name
			}
		}
	}

	for _, e := range r.Entities {
		for _, a := range e.Attributes {
			setEnumName(e, a, a.Type)
		}
	}

	return nil
}

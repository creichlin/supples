// Code generated from parser/Supples.g4 by ANTLR 4.8. DO NOT EDIT.

package parser // Supples

import (
	"fmt"
	"reflect"
	"strconv"

	"github.com/antlr/antlr4/runtime/Go/antlr"
)

// Suppress unused import errors
var _ = fmt.Printf
var _ = reflect.Copy
var _ = strconv.Itoa

var parserATN = []uint16{
	3, 24715, 42794, 33075, 47597, 16764, 15335, 30598, 22884, 3, 29, 130,
	4, 2, 9, 2, 4, 3, 9, 3, 4, 4, 9, 4, 4, 5, 9, 5, 4, 6, 9, 6, 4, 7, 9, 7,
	4, 8, 9, 8, 4, 9, 9, 9, 4, 10, 9, 10, 4, 11, 9, 11, 4, 12, 9, 12, 4, 13,
	9, 13, 4, 14, 9, 14, 4, 15, 9, 15, 4, 16, 9, 16, 4, 17, 9, 17, 4, 18, 9,
	18, 3, 2, 3, 2, 3, 3, 3, 3, 3, 3, 7, 3, 42, 10, 3, 12, 3, 14, 3, 45, 11,
	3, 3, 4, 3, 4, 3, 4, 7, 4, 50, 10, 4, 12, 4, 14, 4, 53, 11, 4, 3, 5, 7,
	5, 56, 10, 5, 12, 5, 14, 5, 59, 11, 5, 3, 5, 3, 5, 3, 6, 3, 6, 3, 6, 3,
	6, 3, 6, 5, 6, 68, 10, 6, 3, 6, 3, 6, 7, 6, 72, 10, 6, 12, 6, 14, 6, 75,
	11, 6, 3, 6, 3, 6, 3, 7, 3, 7, 5, 7, 81, 10, 7, 3, 7, 3, 7, 5, 7, 85, 10,
	7, 3, 8, 3, 8, 3, 8, 3, 8, 5, 8, 91, 10, 8, 3, 9, 3, 9, 3, 9, 3, 9, 5,
	9, 97, 10, 9, 3, 10, 3, 10, 3, 11, 3, 11, 3, 12, 3, 12, 3, 13, 3, 13, 3,
	14, 3, 14, 3, 15, 3, 15, 3, 15, 3, 15, 3, 15, 6, 15, 114, 10, 15, 13, 15,
	14, 15, 115, 3, 15, 3, 15, 3, 16, 3, 16, 3, 16, 5, 16, 123, 10, 16, 3,
	17, 3, 17, 3, 18, 3, 18, 3, 18, 3, 18, 2, 2, 19, 2, 4, 6, 8, 10, 12, 14,
	16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 2, 4, 4, 2, 3, 14, 26, 27, 3, 2,
	16, 17, 2, 127, 2, 36, 3, 2, 2, 2, 4, 38, 3, 2, 2, 2, 6, 46, 3, 2, 2, 2,
	8, 57, 3, 2, 2, 2, 10, 62, 3, 2, 2, 2, 12, 78, 3, 2, 2, 2, 14, 90, 3, 2,
	2, 2, 16, 96, 3, 2, 2, 2, 18, 98, 3, 2, 2, 2, 20, 100, 3, 2, 2, 2, 22,
	102, 3, 2, 2, 2, 24, 104, 3, 2, 2, 2, 26, 106, 3, 2, 2, 2, 28, 108, 3,
	2, 2, 2, 30, 119, 3, 2, 2, 2, 32, 124, 3, 2, 2, 2, 34, 126, 3, 2, 2, 2,
	36, 37, 9, 2, 2, 2, 37, 3, 3, 2, 2, 2, 38, 43, 5, 2, 2, 2, 39, 40, 7, 15,
	2, 2, 40, 42, 5, 2, 2, 2, 41, 39, 3, 2, 2, 2, 42, 45, 3, 2, 2, 2, 43, 41,
	3, 2, 2, 2, 43, 44, 3, 2, 2, 2, 44, 5, 3, 2, 2, 2, 45, 43, 3, 2, 2, 2,
	46, 51, 5, 2, 2, 2, 47, 48, 9, 3, 2, 2, 48, 50, 5, 2, 2, 2, 49, 47, 3,
	2, 2, 2, 50, 53, 3, 2, 2, 2, 51, 49, 3, 2, 2, 2, 51, 52, 3, 2, 2, 2, 52,
	7, 3, 2, 2, 2, 53, 51, 3, 2, 2, 2, 54, 56, 5, 10, 6, 2, 55, 54, 3, 2, 2,
	2, 56, 59, 3, 2, 2, 2, 57, 55, 3, 2, 2, 2, 57, 58, 3, 2, 2, 2, 58, 60,
	3, 2, 2, 2, 59, 57, 3, 2, 2, 2, 60, 61, 7, 2, 2, 3, 61, 9, 3, 2, 2, 2,
	62, 63, 7, 9, 2, 2, 63, 67, 5, 2, 2, 2, 64, 65, 7, 10, 2, 2, 65, 66, 7,
	11, 2, 2, 66, 68, 5, 4, 3, 2, 67, 64, 3, 2, 2, 2, 67, 68, 3, 2, 2, 2, 68,
	69, 3, 2, 2, 2, 69, 73, 7, 18, 2, 2, 70, 72, 5, 12, 7, 2, 71, 70, 3, 2,
	2, 2, 72, 75, 3, 2, 2, 2, 73, 71, 3, 2, 2, 2, 73, 74, 3, 2, 2, 2, 74, 76,
	3, 2, 2, 2, 75, 73, 3, 2, 2, 2, 76, 77, 7, 19, 2, 2, 77, 11, 3, 2, 2, 2,
	78, 84, 5, 2, 2, 2, 79, 81, 7, 26, 2, 2, 80, 79, 3, 2, 2, 2, 80, 81, 3,
	2, 2, 2, 81, 82, 3, 2, 2, 2, 82, 85, 5, 14, 8, 2, 83, 85, 5, 16, 9, 2,
	84, 80, 3, 2, 2, 2, 84, 83, 3, 2, 2, 2, 85, 13, 3, 2, 2, 2, 86, 91, 5,
	18, 10, 2, 87, 91, 5, 22, 12, 2, 88, 91, 5, 24, 13, 2, 89, 91, 5, 32, 17,
	2, 90, 86, 3, 2, 2, 2, 90, 87, 3, 2, 2, 2, 90, 88, 3, 2, 2, 2, 90, 89,
	3, 2, 2, 2, 91, 15, 3, 2, 2, 2, 92, 97, 5, 20, 11, 2, 93, 97, 5, 26, 14,
	2, 94, 97, 5, 28, 15, 2, 95, 97, 5, 34, 18, 2, 96, 92, 3, 2, 2, 2, 96,
	93, 3, 2, 2, 2, 96, 94, 3, 2, 2, 2, 96, 95, 3, 2, 2, 2, 97, 17, 3, 2, 2,
	2, 98, 99, 7, 3, 2, 2, 99, 19, 3, 2, 2, 2, 100, 101, 7, 20, 2, 2, 101,
	21, 3, 2, 2, 2, 102, 103, 7, 4, 2, 2, 103, 23, 3, 2, 2, 2, 104, 105, 7,
	5, 2, 2, 105, 25, 3, 2, 2, 2, 106, 107, 7, 7, 2, 2, 107, 27, 3, 2, 2, 2,
	108, 109, 7, 6, 2, 2, 109, 110, 7, 21, 2, 2, 110, 113, 5, 30, 16, 2, 111,
	112, 7, 15, 2, 2, 112, 114, 5, 30, 16, 2, 113, 111, 3, 2, 2, 2, 114, 115,
	3, 2, 2, 2, 115, 113, 3, 2, 2, 2, 115, 116, 3, 2, 2, 2, 116, 117, 3, 2,
	2, 2, 117, 118, 7, 22, 2, 2, 118, 29, 3, 2, 2, 2, 119, 122, 5, 2, 2, 2,
	120, 121, 7, 23, 2, 2, 121, 123, 7, 28, 2, 2, 122, 120, 3, 2, 2, 2, 122,
	123, 3, 2, 2, 2, 123, 31, 3, 2, 2, 2, 124, 125, 7, 8, 2, 2, 125, 33, 3,
	2, 2, 2, 126, 127, 7, 24, 2, 2, 127, 128, 5, 2, 2, 2, 128, 35, 3, 2, 2,
	2, 13, 43, 51, 57, 67, 73, 80, 84, 90, 96, 115, 122,
}
var deserializer = antlr.NewATNDeserializer(nil)
var deserializedATN = deserializer.DeserializeFromUInt16(parserATN)

var literalNames = []string{
	"", "'int'", "'float'", "'string'", "'enum'", "'bool'", "'time'", "'entity'",
	"'identified'", "'by'", "'name'", "'interfacePackage'", "'postgresPackage'",
	"','", "'.'", "'/'", "'{'", "'}'", "'serial'", "'('", "')'", "'='", "'->'",
	"':'", "'unique'",
}
var symbolicNames = []string{
	"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
	"", "", "", "", "", "COL", "UNIQUE", "IDENTIFIER", "STRING", "WHITESPACE",
}

var ruleNames = []string{
	"identifier", "identifiers", "pkg", "root", "entity", "attribute", "uniqueTypeDef",
	"typeDef", "intType", "serialType", "floatType", "stringType", "boolType",
	"enumType", "enumValue", "timeType", "referenceType",
}
var decisionToDFA = make([]*antlr.DFA, len(deserializedATN.DecisionToState))

func init() {
	for index, ds := range deserializedATN.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(ds, index)
	}
}

type SupplesParser struct {
	*antlr.BaseParser
}

func NewSupplesParser(input antlr.TokenStream) *SupplesParser {
	this := new(SupplesParser)

	this.BaseParser = antlr.NewBaseParser(input)

	this.Interpreter = antlr.NewParserATNSimulator(this, deserializedATN, decisionToDFA, antlr.NewPredictionContextCache())
	this.RuleNames = ruleNames
	this.LiteralNames = literalNames
	this.SymbolicNames = symbolicNames
	this.GrammarFileName = "Supples.g4"

	return this
}

// SupplesParser tokens.
const (
	SupplesParserEOF        = antlr.TokenEOF
	SupplesParserT__0       = 1
	SupplesParserT__1       = 2
	SupplesParserT__2       = 3
	SupplesParserT__3       = 4
	SupplesParserT__4       = 5
	SupplesParserT__5       = 6
	SupplesParserT__6       = 7
	SupplesParserT__7       = 8
	SupplesParserT__8       = 9
	SupplesParserT__9       = 10
	SupplesParserT__10      = 11
	SupplesParserT__11      = 12
	SupplesParserT__12      = 13
	SupplesParserT__13      = 14
	SupplesParserT__14      = 15
	SupplesParserT__15      = 16
	SupplesParserT__16      = 17
	SupplesParserT__17      = 18
	SupplesParserT__18      = 19
	SupplesParserT__19      = 20
	SupplesParserT__20      = 21
	SupplesParserT__21      = 22
	SupplesParserCOL        = 23
	SupplesParserUNIQUE     = 24
	SupplesParserIDENTIFIER = 25
	SupplesParserSTRING     = 26
	SupplesParserWHITESPACE = 27
)

// SupplesParser rules.
const (
	SupplesParserRULE_identifier    = 0
	SupplesParserRULE_identifiers   = 1
	SupplesParserRULE_pkg           = 2
	SupplesParserRULE_root          = 3
	SupplesParserRULE_entity        = 4
	SupplesParserRULE_attribute     = 5
	SupplesParserRULE_uniqueTypeDef = 6
	SupplesParserRULE_typeDef       = 7
	SupplesParserRULE_intType       = 8
	SupplesParserRULE_serialType    = 9
	SupplesParserRULE_floatType     = 10
	SupplesParserRULE_stringType    = 11
	SupplesParserRULE_boolType      = 12
	SupplesParserRULE_enumType      = 13
	SupplesParserRULE_enumValue     = 14
	SupplesParserRULE_timeType      = 15
	SupplesParserRULE_referenceType = 16
)

// IIdentifierContext is an interface to support dynamic dispatch.
type IIdentifierContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsIdentifierContext differentiates from other interfaces.
	IsIdentifierContext()
}

type IdentifierContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIdentifierContext() *IdentifierContext {
	var p = new(IdentifierContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = SupplesParserRULE_identifier
	return p
}

func (*IdentifierContext) IsIdentifierContext() {}

func NewIdentifierContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IdentifierContext {
	var p = new(IdentifierContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = SupplesParserRULE_identifier

	return p
}

func (s *IdentifierContext) GetParser() antlr.Parser { return s.parser }

func (s *IdentifierContext) IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(SupplesParserIDENTIFIER, 0)
}

func (s *IdentifierContext) UNIQUE() antlr.TerminalNode {
	return s.GetToken(SupplesParserUNIQUE, 0)
}

func (s *IdentifierContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IdentifierContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IdentifierContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case SupplesVisitor:
		return t.VisitIdentifier(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *SupplesParser) Identifier() (localctx IIdentifierContext) {
	localctx = NewIdentifierContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 0, SupplesParserRULE_identifier)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(34)
		_la = p.GetTokenStream().LA(1)

		if !(((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<SupplesParserT__0)|(1<<SupplesParserT__1)|(1<<SupplesParserT__2)|(1<<SupplesParserT__3)|(1<<SupplesParserT__4)|(1<<SupplesParserT__5)|(1<<SupplesParserT__6)|(1<<SupplesParserT__7)|(1<<SupplesParserT__8)|(1<<SupplesParserT__9)|(1<<SupplesParserT__10)|(1<<SupplesParserT__11)|(1<<SupplesParserUNIQUE)|(1<<SupplesParserIDENTIFIER))) != 0) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

	return localctx
}

// IIdentifiersContext is an interface to support dynamic dispatch.
type IIdentifiersContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsIdentifiersContext differentiates from other interfaces.
	IsIdentifiersContext()
}

type IdentifiersContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIdentifiersContext() *IdentifiersContext {
	var p = new(IdentifiersContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = SupplesParserRULE_identifiers
	return p
}

func (*IdentifiersContext) IsIdentifiersContext() {}

func NewIdentifiersContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IdentifiersContext {
	var p = new(IdentifiersContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = SupplesParserRULE_identifiers

	return p
}

func (s *IdentifiersContext) GetParser() antlr.Parser { return s.parser }

func (s *IdentifiersContext) AllIdentifier() []IIdentifierContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IIdentifierContext)(nil)).Elem())
	var tst = make([]IIdentifierContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IIdentifierContext)
		}
	}

	return tst
}

func (s *IdentifiersContext) Identifier(i int) IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *IdentifiersContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IdentifiersContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IdentifiersContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case SupplesVisitor:
		return t.VisitIdentifiers(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *SupplesParser) Identifiers() (localctx IIdentifiersContext) {
	localctx = NewIdentifiersContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 2, SupplesParserRULE_identifiers)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(36)
		p.Identifier()
	}
	p.SetState(41)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for _la == SupplesParserT__12 {
		{
			p.SetState(37)
			p.Match(SupplesParserT__12)
		}
		{
			p.SetState(38)
			p.Identifier()
		}

		p.SetState(43)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// IPkgContext is an interface to support dynamic dispatch.
type IPkgContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsPkgContext differentiates from other interfaces.
	IsPkgContext()
}

type PkgContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyPkgContext() *PkgContext {
	var p = new(PkgContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = SupplesParserRULE_pkg
	return p
}

func (*PkgContext) IsPkgContext() {}

func NewPkgContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *PkgContext {
	var p = new(PkgContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = SupplesParserRULE_pkg

	return p
}

func (s *PkgContext) GetParser() antlr.Parser { return s.parser }

func (s *PkgContext) AllIdentifier() []IIdentifierContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IIdentifierContext)(nil)).Elem())
	var tst = make([]IIdentifierContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IIdentifierContext)
		}
	}

	return tst
}

func (s *PkgContext) Identifier(i int) IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *PkgContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PkgContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *PkgContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case SupplesVisitor:
		return t.VisitPkg(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *SupplesParser) Pkg() (localctx IPkgContext) {
	localctx = NewPkgContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 4, SupplesParserRULE_pkg)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(44)
		p.Identifier()
	}
	p.SetState(49)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for _la == SupplesParserT__13 || _la == SupplesParserT__14 {
		{
			p.SetState(45)
			_la = p.GetTokenStream().LA(1)

			if !(_la == SupplesParserT__13 || _la == SupplesParserT__14) {
				p.GetErrorHandler().RecoverInline(p)
			} else {
				p.GetErrorHandler().ReportMatch(p)
				p.Consume()
			}
		}
		{
			p.SetState(46)
			p.Identifier()
		}

		p.SetState(51)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// IRootContext is an interface to support dynamic dispatch.
type IRootContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsRootContext differentiates from other interfaces.
	IsRootContext()
}

type RootContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyRootContext() *RootContext {
	var p = new(RootContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = SupplesParserRULE_root
	return p
}

func (*RootContext) IsRootContext() {}

func NewRootContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *RootContext {
	var p = new(RootContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = SupplesParserRULE_root

	return p
}

func (s *RootContext) GetParser() antlr.Parser { return s.parser }

func (s *RootContext) EOF() antlr.TerminalNode {
	return s.GetToken(SupplesParserEOF, 0)
}

func (s *RootContext) AllEntity() []IEntityContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IEntityContext)(nil)).Elem())
	var tst = make([]IEntityContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IEntityContext)
		}
	}

	return tst
}

func (s *RootContext) Entity(i int) IEntityContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IEntityContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IEntityContext)
}

func (s *RootContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *RootContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *RootContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case SupplesVisitor:
		return t.VisitRoot(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *SupplesParser) Root() (localctx IRootContext) {
	localctx = NewRootContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 6, SupplesParserRULE_root)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(55)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for _la == SupplesParserT__6 {
		{
			p.SetState(52)
			p.Entity()
		}

		p.SetState(57)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(58)
		p.Match(SupplesParserEOF)
	}

	return localctx
}

// IEntityContext is an interface to support dynamic dispatch.
type IEntityContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsEntityContext differentiates from other interfaces.
	IsEntityContext()
}

type EntityContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyEntityContext() *EntityContext {
	var p = new(EntityContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = SupplesParserRULE_entity
	return p
}

func (*EntityContext) IsEntityContext() {}

func NewEntityContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *EntityContext {
	var p = new(EntityContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = SupplesParserRULE_entity

	return p
}

func (s *EntityContext) GetParser() antlr.Parser { return s.parser }

func (s *EntityContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *EntityContext) Identifiers() IIdentifiersContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifiersContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifiersContext)
}

func (s *EntityContext) AllAttribute() []IAttributeContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IAttributeContext)(nil)).Elem())
	var tst = make([]IAttributeContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IAttributeContext)
		}
	}

	return tst
}

func (s *EntityContext) Attribute(i int) IAttributeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAttributeContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IAttributeContext)
}

func (s *EntityContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EntityContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *EntityContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case SupplesVisitor:
		return t.VisitEntity(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *SupplesParser) Entity() (localctx IEntityContext) {
	localctx = NewEntityContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 8, SupplesParserRULE_entity)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(60)
		p.Match(SupplesParserT__6)
	}
	{
		p.SetState(61)
		p.Identifier()
	}
	p.SetState(65)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == SupplesParserT__7 {
		{
			p.SetState(62)
			p.Match(SupplesParserT__7)
		}
		{
			p.SetState(63)
			p.Match(SupplesParserT__8)
		}
		{
			p.SetState(64)
			p.Identifiers()
		}

	}
	{
		p.SetState(67)
		p.Match(SupplesParserT__15)
	}
	p.SetState(71)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for ((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<SupplesParserT__0)|(1<<SupplesParserT__1)|(1<<SupplesParserT__2)|(1<<SupplesParserT__3)|(1<<SupplesParserT__4)|(1<<SupplesParserT__5)|(1<<SupplesParserT__6)|(1<<SupplesParserT__7)|(1<<SupplesParserT__8)|(1<<SupplesParserT__9)|(1<<SupplesParserT__10)|(1<<SupplesParserT__11)|(1<<SupplesParserUNIQUE)|(1<<SupplesParserIDENTIFIER))) != 0 {
		{
			p.SetState(68)
			p.Attribute()
		}

		p.SetState(73)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(74)
		p.Match(SupplesParserT__16)
	}

	return localctx
}

// IAttributeContext is an interface to support dynamic dispatch.
type IAttributeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsAttributeContext differentiates from other interfaces.
	IsAttributeContext()
}

type AttributeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyAttributeContext() *AttributeContext {
	var p = new(AttributeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = SupplesParserRULE_attribute
	return p
}

func (*AttributeContext) IsAttributeContext() {}

func NewAttributeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *AttributeContext {
	var p = new(AttributeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = SupplesParserRULE_attribute

	return p
}

func (s *AttributeContext) GetParser() antlr.Parser { return s.parser }

func (s *AttributeContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *AttributeContext) UniqueTypeDef() IUniqueTypeDefContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IUniqueTypeDefContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IUniqueTypeDefContext)
}

func (s *AttributeContext) TypeDef() ITypeDefContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITypeDefContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITypeDefContext)
}

func (s *AttributeContext) UNIQUE() antlr.TerminalNode {
	return s.GetToken(SupplesParserUNIQUE, 0)
}

func (s *AttributeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *AttributeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *AttributeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case SupplesVisitor:
		return t.VisitAttribute(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *SupplesParser) Attribute() (localctx IAttributeContext) {
	localctx = NewAttributeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 10, SupplesParserRULE_attribute)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(76)
		p.Identifier()
	}
	p.SetState(82)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case SupplesParserT__0, SupplesParserT__1, SupplesParserT__2, SupplesParserT__5, SupplesParserUNIQUE:
		p.SetState(78)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if _la == SupplesParserUNIQUE {
			{
				p.SetState(77)
				p.Match(SupplesParserUNIQUE)
			}

		}
		{
			p.SetState(80)
			p.UniqueTypeDef()
		}

	case SupplesParserT__3, SupplesParserT__4, SupplesParserT__17, SupplesParserT__21:
		{
			p.SetState(81)
			p.TypeDef()
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

// IUniqueTypeDefContext is an interface to support dynamic dispatch.
type IUniqueTypeDefContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsUniqueTypeDefContext differentiates from other interfaces.
	IsUniqueTypeDefContext()
}

type UniqueTypeDefContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyUniqueTypeDefContext() *UniqueTypeDefContext {
	var p = new(UniqueTypeDefContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = SupplesParserRULE_uniqueTypeDef
	return p
}

func (*UniqueTypeDefContext) IsUniqueTypeDefContext() {}

func NewUniqueTypeDefContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *UniqueTypeDefContext {
	var p = new(UniqueTypeDefContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = SupplesParserRULE_uniqueTypeDef

	return p
}

func (s *UniqueTypeDefContext) GetParser() antlr.Parser { return s.parser }

func (s *UniqueTypeDefContext) IntType() IIntTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIntTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIntTypeContext)
}

func (s *UniqueTypeDefContext) FloatType() IFloatTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFloatTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IFloatTypeContext)
}

func (s *UniqueTypeDefContext) StringType() IStringTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IStringTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IStringTypeContext)
}

func (s *UniqueTypeDefContext) TimeType() ITimeTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITimeTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITimeTypeContext)
}

func (s *UniqueTypeDefContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *UniqueTypeDefContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *UniqueTypeDefContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case SupplesVisitor:
		return t.VisitUniqueTypeDef(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *SupplesParser) UniqueTypeDef() (localctx IUniqueTypeDefContext) {
	localctx = NewUniqueTypeDefContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 12, SupplesParserRULE_uniqueTypeDef)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(88)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case SupplesParserT__0:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(84)
			p.IntType()
		}

	case SupplesParserT__1:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(85)
			p.FloatType()
		}

	case SupplesParserT__2:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(86)
			p.StringType()
		}

	case SupplesParserT__5:
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(87)
			p.TimeType()
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

// ITypeDefContext is an interface to support dynamic dispatch.
type ITypeDefContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsTypeDefContext differentiates from other interfaces.
	IsTypeDefContext()
}

type TypeDefContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTypeDefContext() *TypeDefContext {
	var p = new(TypeDefContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = SupplesParserRULE_typeDef
	return p
}

func (*TypeDefContext) IsTypeDefContext() {}

func NewTypeDefContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TypeDefContext {
	var p = new(TypeDefContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = SupplesParserRULE_typeDef

	return p
}

func (s *TypeDefContext) GetParser() antlr.Parser { return s.parser }

func (s *TypeDefContext) SerialType() ISerialTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISerialTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISerialTypeContext)
}

func (s *TypeDefContext) BoolType() IBoolTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBoolTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBoolTypeContext)
}

func (s *TypeDefContext) EnumType() IEnumTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IEnumTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IEnumTypeContext)
}

func (s *TypeDefContext) ReferenceType() IReferenceTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IReferenceTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IReferenceTypeContext)
}

func (s *TypeDefContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TypeDefContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TypeDefContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case SupplesVisitor:
		return t.VisitTypeDef(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *SupplesParser) TypeDef() (localctx ITypeDefContext) {
	localctx = NewTypeDefContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 14, SupplesParserRULE_typeDef)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(94)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case SupplesParserT__17:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(90)
			p.SerialType()
		}

	case SupplesParserT__4:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(91)
			p.BoolType()
		}

	case SupplesParserT__3:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(92)
			p.EnumType()
		}

	case SupplesParserT__21:
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(93)
			p.ReferenceType()
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

// IIntTypeContext is an interface to support dynamic dispatch.
type IIntTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsIntTypeContext differentiates from other interfaces.
	IsIntTypeContext()
}

type IntTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIntTypeContext() *IntTypeContext {
	var p = new(IntTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = SupplesParserRULE_intType
	return p
}

func (*IntTypeContext) IsIntTypeContext() {}

func NewIntTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IntTypeContext {
	var p = new(IntTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = SupplesParserRULE_intType

	return p
}

func (s *IntTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *IntTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IntTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IntTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case SupplesVisitor:
		return t.VisitIntType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *SupplesParser) IntType() (localctx IIntTypeContext) {
	localctx = NewIntTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 16, SupplesParserRULE_intType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(96)
		p.Match(SupplesParserT__0)
	}

	return localctx
}

// ISerialTypeContext is an interface to support dynamic dispatch.
type ISerialTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSerialTypeContext differentiates from other interfaces.
	IsSerialTypeContext()
}

type SerialTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySerialTypeContext() *SerialTypeContext {
	var p = new(SerialTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = SupplesParserRULE_serialType
	return p
}

func (*SerialTypeContext) IsSerialTypeContext() {}

func NewSerialTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SerialTypeContext {
	var p = new(SerialTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = SupplesParserRULE_serialType

	return p
}

func (s *SerialTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *SerialTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SerialTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SerialTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case SupplesVisitor:
		return t.VisitSerialType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *SupplesParser) SerialType() (localctx ISerialTypeContext) {
	localctx = NewSerialTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 18, SupplesParserRULE_serialType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(98)
		p.Match(SupplesParserT__17)
	}

	return localctx
}

// IFloatTypeContext is an interface to support dynamic dispatch.
type IFloatTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsFloatTypeContext differentiates from other interfaces.
	IsFloatTypeContext()
}

type FloatTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyFloatTypeContext() *FloatTypeContext {
	var p = new(FloatTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = SupplesParserRULE_floatType
	return p
}

func (*FloatTypeContext) IsFloatTypeContext() {}

func NewFloatTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FloatTypeContext {
	var p = new(FloatTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = SupplesParserRULE_floatType

	return p
}

func (s *FloatTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *FloatTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FloatTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FloatTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case SupplesVisitor:
		return t.VisitFloatType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *SupplesParser) FloatType() (localctx IFloatTypeContext) {
	localctx = NewFloatTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 20, SupplesParserRULE_floatType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(100)
		p.Match(SupplesParserT__1)
	}

	return localctx
}

// IStringTypeContext is an interface to support dynamic dispatch.
type IStringTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsStringTypeContext differentiates from other interfaces.
	IsStringTypeContext()
}

type StringTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStringTypeContext() *StringTypeContext {
	var p = new(StringTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = SupplesParserRULE_stringType
	return p
}

func (*StringTypeContext) IsStringTypeContext() {}

func NewStringTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StringTypeContext {
	var p = new(StringTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = SupplesParserRULE_stringType

	return p
}

func (s *StringTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *StringTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StringTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StringTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case SupplesVisitor:
		return t.VisitStringType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *SupplesParser) StringType() (localctx IStringTypeContext) {
	localctx = NewStringTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 22, SupplesParserRULE_stringType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(102)
		p.Match(SupplesParserT__2)
	}

	return localctx
}

// IBoolTypeContext is an interface to support dynamic dispatch.
type IBoolTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsBoolTypeContext differentiates from other interfaces.
	IsBoolTypeContext()
}

type BoolTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyBoolTypeContext() *BoolTypeContext {
	var p = new(BoolTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = SupplesParserRULE_boolType
	return p
}

func (*BoolTypeContext) IsBoolTypeContext() {}

func NewBoolTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BoolTypeContext {
	var p = new(BoolTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = SupplesParserRULE_boolType

	return p
}

func (s *BoolTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *BoolTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BoolTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *BoolTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case SupplesVisitor:
		return t.VisitBoolType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *SupplesParser) BoolType() (localctx IBoolTypeContext) {
	localctx = NewBoolTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 24, SupplesParserRULE_boolType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(104)
		p.Match(SupplesParserT__4)
	}

	return localctx
}

// IEnumTypeContext is an interface to support dynamic dispatch.
type IEnumTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsEnumTypeContext differentiates from other interfaces.
	IsEnumTypeContext()
}

type EnumTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyEnumTypeContext() *EnumTypeContext {
	var p = new(EnumTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = SupplesParserRULE_enumType
	return p
}

func (*EnumTypeContext) IsEnumTypeContext() {}

func NewEnumTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *EnumTypeContext {
	var p = new(EnumTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = SupplesParserRULE_enumType

	return p
}

func (s *EnumTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *EnumTypeContext) AllEnumValue() []IEnumValueContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IEnumValueContext)(nil)).Elem())
	var tst = make([]IEnumValueContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IEnumValueContext)
		}
	}

	return tst
}

func (s *EnumTypeContext) EnumValue(i int) IEnumValueContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IEnumValueContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IEnumValueContext)
}

func (s *EnumTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EnumTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *EnumTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case SupplesVisitor:
		return t.VisitEnumType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *SupplesParser) EnumType() (localctx IEnumTypeContext) {
	localctx = NewEnumTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 26, SupplesParserRULE_enumType)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(106)
		p.Match(SupplesParserT__3)
	}
	{
		p.SetState(107)
		p.Match(SupplesParserT__18)
	}
	{
		p.SetState(108)
		p.EnumValue()
	}
	p.SetState(111)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for ok := true; ok; ok = _la == SupplesParserT__12 {
		{
			p.SetState(109)
			p.Match(SupplesParserT__12)
		}
		{
			p.SetState(110)
			p.EnumValue()
		}

		p.SetState(113)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(115)
		p.Match(SupplesParserT__19)
	}

	return localctx
}

// IEnumValueContext is an interface to support dynamic dispatch.
type IEnumValueContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsEnumValueContext differentiates from other interfaces.
	IsEnumValueContext()
}

type EnumValueContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyEnumValueContext() *EnumValueContext {
	var p = new(EnumValueContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = SupplesParserRULE_enumValue
	return p
}

func (*EnumValueContext) IsEnumValueContext() {}

func NewEnumValueContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *EnumValueContext {
	var p = new(EnumValueContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = SupplesParserRULE_enumValue

	return p
}

func (s *EnumValueContext) GetParser() antlr.Parser { return s.parser }

func (s *EnumValueContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *EnumValueContext) STRING() antlr.TerminalNode {
	return s.GetToken(SupplesParserSTRING, 0)
}

func (s *EnumValueContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EnumValueContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *EnumValueContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case SupplesVisitor:
		return t.VisitEnumValue(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *SupplesParser) EnumValue() (localctx IEnumValueContext) {
	localctx = NewEnumValueContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 28, SupplesParserRULE_enumValue)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(117)
		p.Identifier()
	}
	p.SetState(120)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == SupplesParserT__20 {
		{
			p.SetState(118)
			p.Match(SupplesParserT__20)
		}
		{
			p.SetState(119)
			p.Match(SupplesParserSTRING)
		}

	}

	return localctx
}

// ITimeTypeContext is an interface to support dynamic dispatch.
type ITimeTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsTimeTypeContext differentiates from other interfaces.
	IsTimeTypeContext()
}

type TimeTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTimeTypeContext() *TimeTypeContext {
	var p = new(TimeTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = SupplesParserRULE_timeType
	return p
}

func (*TimeTypeContext) IsTimeTypeContext() {}

func NewTimeTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TimeTypeContext {
	var p = new(TimeTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = SupplesParserRULE_timeType

	return p
}

func (s *TimeTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *TimeTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TimeTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TimeTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case SupplesVisitor:
		return t.VisitTimeType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *SupplesParser) TimeType() (localctx ITimeTypeContext) {
	localctx = NewTimeTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 30, SupplesParserRULE_timeType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(122)
		p.Match(SupplesParserT__5)
	}

	return localctx
}

// IReferenceTypeContext is an interface to support dynamic dispatch.
type IReferenceTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsReferenceTypeContext differentiates from other interfaces.
	IsReferenceTypeContext()
}

type ReferenceTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyReferenceTypeContext() *ReferenceTypeContext {
	var p = new(ReferenceTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = SupplesParserRULE_referenceType
	return p
}

func (*ReferenceTypeContext) IsReferenceTypeContext() {}

func NewReferenceTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ReferenceTypeContext {
	var p = new(ReferenceTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = SupplesParserRULE_referenceType

	return p
}

func (s *ReferenceTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *ReferenceTypeContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *ReferenceTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ReferenceTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ReferenceTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case SupplesVisitor:
		return t.VisitReferenceType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *SupplesParser) ReferenceType() (localctx IReferenceTypeContext) {
	localctx = NewReferenceTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 32, SupplesParserRULE_referenceType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(124)
		p.Match(SupplesParserT__21)
	}
	{
		p.SetState(125)
		p.Identifier()
	}

	return localctx
}

// Code generated from parser/Supples.g4 by ANTLR 4.8. DO NOT EDIT.

package parser // Supples

import "github.com/antlr/antlr4/runtime/Go/antlr"

// A complete Visitor for a parse tree produced by SupplesParser.
type SupplesVisitor interface {
	antlr.ParseTreeVisitor

	// Visit a parse tree produced by SupplesParser#identifier.
	VisitIdentifier(ctx *IdentifierContext) interface{}

	// Visit a parse tree produced by SupplesParser#identifiers.
	VisitIdentifiers(ctx *IdentifiersContext) interface{}

	// Visit a parse tree produced by SupplesParser#pkg.
	VisitPkg(ctx *PkgContext) interface{}

	// Visit a parse tree produced by SupplesParser#root.
	VisitRoot(ctx *RootContext) interface{}

	// Visit a parse tree produced by SupplesParser#entity.
	VisitEntity(ctx *EntityContext) interface{}

	// Visit a parse tree produced by SupplesParser#attribute.
	VisitAttribute(ctx *AttributeContext) interface{}

	// Visit a parse tree produced by SupplesParser#uniqueTypeDef.
	VisitUniqueTypeDef(ctx *UniqueTypeDefContext) interface{}

	// Visit a parse tree produced by SupplesParser#typeDef.
	VisitTypeDef(ctx *TypeDefContext) interface{}

	// Visit a parse tree produced by SupplesParser#intType.
	VisitIntType(ctx *IntTypeContext) interface{}

	// Visit a parse tree produced by SupplesParser#serialType.
	VisitSerialType(ctx *SerialTypeContext) interface{}

	// Visit a parse tree produced by SupplesParser#floatType.
	VisitFloatType(ctx *FloatTypeContext) interface{}

	// Visit a parse tree produced by SupplesParser#stringType.
	VisitStringType(ctx *StringTypeContext) interface{}

	// Visit a parse tree produced by SupplesParser#boolType.
	VisitBoolType(ctx *BoolTypeContext) interface{}

	// Visit a parse tree produced by SupplesParser#enumType.
	VisitEnumType(ctx *EnumTypeContext) interface{}

	// Visit a parse tree produced by SupplesParser#enumValue.
	VisitEnumValue(ctx *EnumValueContext) interface{}

	// Visit a parse tree produced by SupplesParser#timeType.
	VisitTimeType(ctx *TimeTypeContext) interface{}

	// Visit a parse tree produced by SupplesParser#referenceType.
	VisitReferenceType(ctx *ReferenceTypeContext) interface{}
}

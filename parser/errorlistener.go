package parser

import (
	"errors"
	"strconv"
	"strings"

	"github.com/antlr/antlr4/runtime/Go/antlr"
)

type ErrorListener struct {
	errors []error
}

func NewErrorListener() *ErrorListener {
	return new(ErrorListener)
}

func (d *ErrorListener) Get() error {
	if len(d.errors) == 0 {
		return nil
	}
	return d
}

func (d *ErrorListener) Error() string {
	e := []string{}
	for _, err := range d.errors {
		e = append(e, err.Error())
	}
	return strings.Join(e, "\n")
}

func (d *ErrorListener) SyntaxError(recognizer antlr.Recognizer, offendingSymbol interface{}, line, column int, msg string, e antlr.RecognitionException) {
	d.errors = append(d.errors, errors.New(strconv.Itoa(line)+":"+strconv.Itoa(column)+" "+msg))
}

func (d *ErrorListener) ReportAmbiguity(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex int, exact bool, ambigAlts *antlr.BitSet, configs antlr.ATNConfigSet) {

}

func (d *ErrorListener) ReportAttemptingFullContext(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex int, conflictingAlts *antlr.BitSet, configs antlr.ATNConfigSet) {
}

func (d *ErrorListener) ReportContextSensitivity(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex, prediction int, configs antlr.ATNConfigSet) {
}
